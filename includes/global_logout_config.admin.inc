<?php
/**
 * @file
 * Define Administrator form to configure global logout.
 */

/**
 * Implements hook_form().
 */
function global_logout_settings_form($form, &$form_state) {
  $form['global_logout_menu_name'] = array(
    '#type' => 'textfield',
    '#size' => 120,
    '#maxlength' => 250,
    '#required' => TRUE,
    '#title' => t('Logout menu name'),
    '#description' => t("Enter the menu name to be displayed for global logout at user-menu. Flush the menu cache if changed"),
    '#default_value' => variable_get('global_logout_menu_name', 'Global Logout'),
  );

  $cur_format = variable_get('global_logout_access_time_format', 'Y-m-d H:i');

  $form['global_logout_access_time_format'] = array(
    '#type' => 'textfield',
    '#size' => 120,
    '#maxlength' => 250,
    '#title' => 'Time format',
    '#required' => TRUE,
    '#description' => t('Provide a valid format to customise time format displayed at Active login. A user-defined date format. See the <a href="@url">PHP manual</a> for available options.', array('@url' => 'http://php.net/manual/function.date.php')),
    '#suffix' => '<div id="global-logout-date-time-format-suffix">Displayed as : ' . date($cur_format, REQUEST_TIME) . '</div>',
    '#ajax' => array(
      'callback' => 'global_logout_timeformat_ajax_callback',
      'wrapper' => 'global-logout-date-time-format-suffix',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#default_value' => variable_get('global_logout_access_time_format', 'Y-m-d H:i'),
  );

  return system_settings_form($form);
}

/**
 * Global Logout date-time format ajax callback.
 */
function global_logout_timeformat_ajax_callback($form, $form_state) {
  $global_logout_cur_val = $form_state['values']['global_logout_access_time_format'];
  $global_logout_cur_format = date($global_logout_cur_val, REQUEST_TIME);
  $global_logout_format_str = '<div id="global-logout-date-time-format-suffix">Displayed as : ' . $global_logout_cur_format . ' </div>';
  return $global_logout_format_str;
}
