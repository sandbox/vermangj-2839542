<?php
/**
 * @file
 * Class for detecting user's browser and platform.
 */

/**
 * Returns Browser and the Platform.
 */
class GlobalLogoutDetectOsBr {
  private $agent = "";
  private $info = array();

  /**
   * Version, browser, and platform object initialized.
   */
  public function __construct() {
    $this->agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
    $this->getBrowser();
    $this->getOs();
  }

  /**
   * Returns Client Browser.
   */
  public function getBrowser() {
    $browser = array(
      "Navigator" => "/Navigator(.*)/i",
      "Firefox" => "/Firefox(.*)/i",
      "Internet Explorer" => "/MSIE(.*)/i",
      "Google Chrome" => "/chrome(.*)/i",
      "MAXTHON" => "/MAXTHON(.*)/i",
      "Opera" => "/Opera(.*)/i",
    );
    foreach ($browser as $key => $value) {
      if (preg_match($value, $this->agent)) {
        $this->info = array_merge($this->info, array(
          "Browser" => $key,
        ));
        $this->info = array_merge($this->info, array(
          "Version" => $this->getVersion($key, $value, $this->agent),
        ));
        break;
      }
      else {
        $this->info = array_merge($this->info, array(
          "Browser" => "UnKnown",
        ));
        $this->info = array_merge($this->info, array(
          "Version" => "UnKnown",
        ));
      }
    }
    return $this->info['Browser'];
  }

  /**
   * Returns Client Platform.
   */
  public function getOs() {
    $os = array(
      "Mobile" => '/Android/i',
      "Windows" => "/Windows/i",
      "Linux" => "/Linux/i",
      "Unix" => "/Unix/i",
      "Mac" => "/Mac/i",
    );

    foreach ($os as $key => $value) {
      if (preg_match($value, $this->agent)) {
        $this->info = array_merge($this->info, array(
          "Operating System" => $key,
        ));
        break;

      }
    }
    return $this->info['Operating System'];
  }

  /**
   * Returns the version of the Browser.
   */
  public function getVersion($browser, $search, $string) {
    $browser = $this->info['Browser'];
    $version = "";
    $browser = strtolower($browser);
    preg_match_all($search, $string, $match);
    switch ($browser) {
      case "firefox":
        $version = str_replace("/", "", $match[1][0]);
        break;

      case "internet explorer":
        $version = substr($match[1][0], 0, 4);
        break;

      case "opera":
        $version = str_replace("/", "", substr($match[1][0], 0, 5));
        break;

      case "navigator":
        $version = substr($match[1][0], 1, 7);
        break;

      case "maxthon":
        $version = str_replace(")", "", $match[1][0]);
        break;

      case "google chrome":
        $version = substr($match[1][0], 1, 10);
    }
    return $version;
  }

  /**
   * Returns the result(browser and platform) based on the parameter passed.
   */
  public function showInfo($switch) {
    $switch = strtolower($switch);
    switch ($switch) {
      case "browser":
        $return_res = $this->info['Browser'];
        break;

      case "os":
        $return_res = $this->info['Operating System'];
        break;

      case "version":
        $return_res = $this->info['Version'];
        break;

      case "all":
        $return_res = array(
          $this->info["Version"],
          $this->info['Operating System'],
          $this->info['Browser'],
        );
        break;

      default:
        $return_res = "Unknown";
        break;

    }
    return $return_res;
  }

}
