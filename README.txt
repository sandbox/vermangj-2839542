Global Logout Module
--------------------

Today with multiple applications being built using Drupal and with user's a
-ccessing Drupal from different devices, a security feature that is must ne
-eded is for the user to be able to monitor and manage the different sessio
-ns through which he is using the application.

This module helps to manage a user's active logins. The user can view his a-
ctive logged in information.

This information includes Hostname, Accessed Time, Browser and the platform
-(OS).

Using session information this module can logout a user from all the device
-s with the active logins. This page is available as a menu under main-menu
-and is built using views.

A menu called Global Logout is added to the User Menu.

Inspired by Google's Activity details feature.

NOTE
----

Flush the cache after enabling global logout module.

Configuration
-------------

Global Logout module is provided with a configuration page. This configurati
-on page is to manage with the "Global Logout" menu name. The time format to
be displayed can be specified.

CONTACT
-------

Current maintainers:
* Anishnirmal D (Anishnirmal) - https://www.drupal.org/user/3100899

Future Enhancements
-------------------

1. Location identifier.
2. Recently accessed device.
3. Mail alert on signing in new devices.
