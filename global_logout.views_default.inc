<?php
/**
 * @file
 * Creates default view for Global Logout to manage user's active session.
 */

/**
 * Implements hook_views_default_views().
 */
function global_logout_views_default_views() {
  $view              = new view();
  $view->name        = 'global_logout';
  $view->description = '';
  $view->tag         = 'default';
  $view->base_table  = 'globallogout_session_info';
  $view->human_name  = 'Global Logout';
  $view->core        = 7;
  $view->api_version = '3.0';
  $view->disabled    = FALSE;
  $handler           = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title']                                      = '<none>';
  $handler->display->display_options['use_more_always']                            = FALSE;
  $handler->display->display_options['access']['type']                             = 'role';
  $handler->display->display_options['access']['role']                             = array(
    2 => '2',
  );
  $handler->display->display_options['cache']['type']                              = 'none';
  $handler->display->display_options['query']['type']                              = 'views_query';
  $handler->display->display_options['query']['options']['distinct']               = TRUE;
  $handler->display->display_options['exposed_form']['type']                       = 'basic';
  $handler->display->display_options['pager']['type']                              = 'full';
  $handler->display->display_options['pager']['options']['items_per_page']         = '10';
  $handler->display->display_options['style_plugin']                               = 'table';
  $handler->display->display_options['style_options']['columns']                   = array(
    'user_access_information' => 'user_access_information',
  );
  $handler->display->display_options['style_options']['default']                   = '-1';
  $handler->display->display_options['style_options']['info']                      = array(
    'user_access_information' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['header']['php']['id']                        = 'php';
  $handler->display->display_options['header']['php']['table']                     = 'views';
  $handler->display->display_options['header']['php']['field']                     = 'php';
  $handler->display->display_options['header']['php']['php_output'] = '<h2>Check which devices have active logins to account</h2>
  <h5>If you don\'t recognize below active login informations, someone may have your password. We recommend you to secure your account now by Global Logout<h5>';
  $handler->display->display_options['empty']['area']['id']                        = 'area';
  $handler->display->display_options['empty']['area']['table']                     = 'views';
  $handler->display->display_options['empty']['area']['field']                     = 'area';
  $handler->display->display_options['empty']['area']['empty']                     = TRUE;
  $handler->display->display_options['empty']['area']['content']                   = 'No active logins found';
  $handler->display->display_options['empty']['area']['format']                    = 'filtered_html';
  $handler->display->display_options['fields']['sid']['id']                        = 'sid';
  $handler->display->display_options['fields']['sid']['table']                     = 'globallogout_session_info';
  $handler->display->display_options['fields']['sid']['field']                     = 'sid';
  $handler->display->display_options['fields']['sid']['exclude']                   = TRUE;
  $handler->display->display_options['fields']['uid']['id']                        = 'uid';
  $handler->display->display_options['fields']['uid']['table']                     = 'globallogout_session_info';
  $handler->display->display_options['fields']['uid']['field']                     = 'uid';
  $handler->display->display_options['fields']['uid']['exclude']                   = TRUE;
  $handler->display->display_options['fields']['counter']['id']                    = 'counter';
  $handler->display->display_options['fields']['counter']['table']                 = 'views';
  $handler->display->display_options['fields']['counter']['field']                 = 'counter';
  $handler->display->display_options['fields']['counter']['label']                 = 'Serial Number';
  $handler->display->display_options['fields']['counter']['counter_start']         = '1';
  $handler->display->display_options['fields']['counter']['reverse']               = 0;
  $handler->display->display_options['fields']['php']['id']                        = 'php';
  $handler->display->display_options['fields']['php']['table']                     = 'views';
  $handler->display->display_options['fields']['php']['field']                     = 'php';
  $handler->display->display_options['fields']['php']['label']                     = 'User name';
  $handler->display->display_options['fields']['php']['use_php_setup']             = 0;
  $handler->display->display_options['fields']['php']['php_output']                = '<?php echo user_load($row->uid)->name;
  ?>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable']    = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable']        = '';
  $handler->display->display_options['fields']['php_1']['id']                      = 'php_1';
  $handler->display->display_options['fields']['php_1']['table']                   = 'views';
  $handler->display->display_options['fields']['php_1']['field']                   = 'php';
  $handler->display->display_options['fields']['php_1']['label']                   = 'Time';
  $handler->display->display_options['fields']['php_1']['use_php_setup']           = 0;
  $handler->display->display_options['fields']['php_1']['php_output']              = '<?php print global_logout_load_session_info($row->uid, $row->sid, \'time\');
  ?>';
  $handler->display->display_options['fields']['php_1']['use_php_click_sortable']  = '0';
  $handler->display->display_options['fields']['php_1']['php_click_sortable']      = '';
  $handler->display->display_options['fields']['user_access_information']['id']    = 'user_access_information';
  $handler->display->display_options['fields']['user_access_information']['table'] = 'globallogout_session_info';
  $handler->display->display_options['fields']['user_access_information']['field'] = 'user_access_information';
  $handler->display->display_options['fields']['user_access_information']['label'] = 'User Access Information';
  $handler->display->display_options['fields']['php_2']['id']                      = 'php_2';
  $handler->display->display_options['fields']['php_2']['table']                   = 'views';
  $handler->display->display_options['fields']['php_2']['field']                   = 'php';
  $handler->display->display_options['fields']['php_2']['label']                   = 'Hostname';
  $handler->display->display_options['fields']['php_2']['use_php_setup']           = 0;
  $handler->display->display_options['fields']['php_2']['php_output']              = '<?php print global_logout_load_session_info($row->uid, $row->sid, \'ip\');
  ?>';
  $handler->display->display_options['fields']['php_2']['use_php_click_sortable']  = '0';
  $handler->display->display_options['fields']['php_2']['php_click_sortable']      = '';
  $handler->display->display_options['filters']['php']['id']                       = 'php';
  $handler->display->display_options['filters']['php']['table']                    = 'views';
  $handler->display->display_options['filters']['php']['field']                    = 'php';
  $handler->display->display_options['filters']['php']['use_php_setup']            = 0;
  $handler->display->display_options['filters']['php']['php_filter']               = 'global $user;
  if($user->uid != $row->uid) {
    return TRUE;
  } else {
    return FALSE;
  }';
  $handler                                                          = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path']                        = 'user-active-login-information';
  $handler->display->display_options['menu']['type']                = 'normal';
  $handler->display->display_options['menu']['title']               = 'Active Logins';
  $handler->display->display_options['menu']['weight']              = '0';
  $handler->display->display_options['menu']['name']                = 'main-menu';
  $handler->display->display_options['menu']['context']             = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler                                                                 = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['pager']                  = FALSE;
  $handler->display->display_options['pager']['type']                      = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $views[$view->name] = $view;
  return $views;
}
